import React from 'react';
import './CountryList.css'

const CountryList = props => {
    return (
        <div onClick={props.clicked} className="country-list">
            {props.name}
        </div>
    );
};

export default CountryList;