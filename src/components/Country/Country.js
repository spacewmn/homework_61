import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Border from "../Border/Border";
import './Country.css'

const COUNTRYINFO_URL = 'https://restcountries.eu/rest/v2/name/';
const BORDER_URL = 'https://restcountries.eu/rest/v2/alpha/';

const Country = props => {
    const [countryInfo, setCountryInfo] = useState(null);
    const [border, setBorder] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            if (props.name !== null) {
                const countryInfoResponse = await axios.get(COUNTRYINFO_URL + props.name);
                const promises = countryInfoResponse.data.map(async country => {
                    return {name: country.name,
                            capital: country.capital,
                            population: country.population,
                            flag: country.flag,
                            borders: country.borders};
                });
                const newCountries = await Promise.all(promises);
                setCountryInfo(newCountries);
                console.log(countryInfo[0].borders);
            };
        };
        fetchData().catch(console.error);
    }, [props.name]);


    return countryInfo && (
        <div className="country-info">
            <h1>{countryInfo[0].name}</h1>
            <img src={countryInfo[0].flag} alt="#" className="flag"/>
            <p>Capital: {countryInfo[0].capital}</p>
            <p>Population: {countryInfo[0].population}</p>
        </div>
    );
};

export default Country;