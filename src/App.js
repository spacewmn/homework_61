import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import CountryList from "./components/CountryList/CountryList";
import Country from "./components/Country/Country";

const COUNTRIES_URL = 'https://restcountries.com/v2/all';
const COUNTRYNAME_URL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
const COUNTRYINFO_URL = 'https://restcountries.eu/rest/v2/alpha/'

const App = () => {
  const [countryName, setCountryName] = useState([]);
  const [selectedCountryCode, setSelectedCountryCode] = useState(null);


  useEffect(() =>{
    const getCountryList = async () => {
      const countriesResponse = await axios.get(COUNTRIES_URL);
      // console.log(countriesResponse.data);
      const promises = countriesResponse.data.map(async country => {
        // const countryList = country.name;
        // console.log(countryList);
        return {name: country.name};
      });
      const newCountries = await Promise.all(promises);
      setCountryName(newCountries);
    };
    getCountryList().catch(console.error);
  }, []);

  return (
      <div className="App">
        <div className="country-name">
          {countryName.map(country => {
              return <CountryList
              key={country.name}
              name={country.name}
              clicked={() => setSelectedCountryCode(country.name)}
            />
          })}
        </div>
        <div>
          <Country name={selectedCountryCode} />
        </div>
      </div>
  );
};

export default App;
